#include "../libs/image.h"

struct image image_create(uint32_t height, uint32_t width) {
    struct image result;
    result.height = height;
    result.width = width;
    result.data = (struct pixel *)malloc(sizeof(struct pixel) * height * width);
    return result;
}

struct image rotate_img(struct image const source) {
    struct image result = image_create(source.width, source.height);
    for (uint64_t i = 0; i < result.height; i++) {
        for (uint64_t j = 0; j < result.width; j++) {
            result.data[i * result.width + j] = source.data[(source.height - j - 1) * source.width + i];
        }
    }
    return result;
}

void image_delete(struct image img) {
    free(img.data);
}

