#include "../libs/bmp.h"
#include "../libs/file.h"
#include "../libs/status_bmp_prints.h"

#include <stdio.h>
#include <stdlib.h>


#define EXPECTED_PARAM_COUNT 3
#define EXCEPTION_CODE 1
#define SUCCESS_CODE 0
#define READ_BYTE_MODE "rb"
#define WRITE_BYTE_MODE "wb"

void print_read_error(enum read_status status) {
    printf("%s", read_error_messages[status]);
}


int main( int argc, char** argv ) {

    if (argc != EXPECTED_PARAM_COUNT) {
        printf("You must pass 3 parameters");
        return EXCEPTION_CODE;
    }

    FILE *f_in = NULL;
    FILE *f_out = NULL;


    enum file_state f_in_state = open_f(&f_in, argv[1], READ_BYTE_MODE);
    if (f_in_state == OPEN_ERROR) {
        printf("Can't open input file");
        free(f_in);
        free(f_out);
        return EXCEPTION_CODE;
    }

    enum file_state f_out_state = open_f(&f_out, argv[2], WRITE_BYTE_MODE);
    if(f_out_state == OPEN_ERROR) {
        printf("Can't open output file");
        free(f_in);
        free(f_out);
        return EXCEPTION_CODE;
    }

    struct image original_pic;

    enum read_status f_in_read_status = from_bmp(f_in, &original_pic);

    if (f_in_read_status != READ_BMP_OK) {
        print_read_error(f_in_read_status);
        fclose(f_in);
        fclose(f_out);
	image_delete(original_pic);
        return EXCEPTION_CODE;
    }

    struct image rotated_image = rotate_img(original_pic);

    image_delete(original_pic);

    enum write_status f_out_write_status = to_bmp(f_out, &rotated_image);

    image_delete(rotated_image);

    if (f_out_write_status == WR_ERROR) {
        printf("Error while writing in output file");
        fclose(f_in);
        fclose(f_out);
        return EXCEPTION_CODE;
    }
    f_in_state = close_f(f_in);
    f_out_state = close_f(f_out);
    if (f_in_state != SUCCESS || f_out_state != SUCCESS) {
        if (f_in_state != SUCCESS) {
            printf("Can't close input file");
        }
        if (f_out_state != SUCCESS) {
            printf("Can't close output file");
        }
        return EXCEPTION_CODE;
    }
    return SUCCESS_CODE;
}

