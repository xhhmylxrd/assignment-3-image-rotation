#include "../libs/file.h"


enum file_state open_f(FILE **f, char const *path, char const *mode) {
    *f = fopen(path, mode);
    return *f == NULL ? OPEN_ERROR : SUCCESS;
}

enum file_state close_f(FILE *f){
    return fclose(f) == 0 ? SUCCESS: CLOSE_ERROR;
}
