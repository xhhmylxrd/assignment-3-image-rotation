#include "../libs/bmp.h"
#include <inttypes.h>
#include <stdio.h>
#define BMP_SIGNATURE 0x4d42
#define PIXEL_IN_BITS 24

#define BMP_HEADER_SIZE sizeof(struct bmp_header)
#define PIXEL_SIZE sizeof(struct pixel)


uint32_t get_padding(uint32_t width) {
    return 4 - (width * 3) % 4;

}


uint32_t get_image_size(uint32_t height, uint32_t width) {
    return (width + get_padding(width)) * height * PIXEL_SIZE;
}

uint32_t get_bmp_size(uint32_t img_size) {
    return (img_size + BMP_HEADER_SIZE);
}



enum read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header header;

    if (fread(&header, BMP_HEADER_SIZE, 1, in) != 1) {
        return READ_INVALID_HEADER;
    }


    if (header.bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != PIXEL_IN_BITS) {
        return READ_INVALID_BITS;
    }

    if (header.biSize <= 0) {
        return READ_INVALID_BMP_SIZE;
    }

    if (header.biWidth <= 0 || header.biHeight <= 0) {
        return READ_INVALID_IMG_SIZE;
    }


    *image = image_create(header.biHeight, header.biWidth);

    if (image->data == NULL) {
        return READ_NULL_POINTER;
    }

    uint32_t padding = get_padding((uint32_t)image->width);


    for (uint64_t i = 0; i < image->height; i++) {
        void *ptr_start = image->data + image->width * i;

        if (fread(ptr_start, PIXEL_SIZE, image->width, in) != image->width) {
            image_delete(*image);
            return READ_INVALID_BODY;
        }

        if (fseek(in, padding, SEEK_CUR)) {
            image_delete(*image);
            return READ_INVALID_BODY;
        }
    }
    return READ_BMP_OK;
}



enum write_status to_bmp(FILE* out, struct image* image) {
    struct bmp_header header;
    uint32_t padding = get_padding((uint32_t) image->width);

    const uint32_t image_size = get_image_size(image->height, image->width);


    const uint32_t file_size = get_bmp_size(image_size);


    header = (struct bmp_header){
            .bfType = BMP_SIGNATURE,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_SIZE,
            .biSize = PIXEL_IN_BITS,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = PIXEL_IN_BITS,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if (fwrite(&header, BMP_HEADER_SIZE, 1, out) == 0) {
        return WR_ERROR;
    }

    for (uint64_t i = 0; i < image->height; i++) {
        void* ptr_start = (image->data + image->width * i);

        if (fwrite(ptr_start, PIXEL_SIZE, image->width, out) == 0) {
            image_delete(*image);
            return WR_ERROR;
        }

        if (fseek( out, padding, SEEK_CUR ) != 0) {
            image_delete(*image);
            return WR_ERROR;
        }
    }
    return WRITE_BMP_OK;
}
