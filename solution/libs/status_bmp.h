#ifndef IMAGE_ROTATION_STATUS_BMP_H
#define IMAGE_ROTATION_STATUS_BMP_H

enum read_status {
    READ_BMP_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_BMP_SIZE,
    READ_INVALID_IMG_SIZE,
    READ_NULL_POINTER,
    READ_INVALID_BODY,
};

enum write_status {
    WRITE_BMP_OK = 0,
    WR_ERROR,
};
#endif
