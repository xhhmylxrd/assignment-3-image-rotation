#ifndef IMAGE_ROTATION_FILE_H
#define IMAGE_ROTATION_FILE_H
#include "stdio.h"
enum file_state {
    SUCCESS = 0,
    OPEN_ERROR,
    CLOSE_ERROR,
};

enum file_state open_f(FILE **f, char const *path, char const *mode);

enum file_state close_f(FILE *f);

#endif
