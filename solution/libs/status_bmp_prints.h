//
// Created by glebv on 07.01.2023.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_STATUS_BMP_PRINTS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_STATUS_BMP_PRINTS_H

const char * read_error_messages[] = {
        [READ_BMP_OK] = "Successfully read\n",
        [READ_INVALID_SIGNATURE] = "Invalid signature of the file\n",
        [READ_INVALID_BITS] = "Invalid biBitCount\n",
        [READ_INVALID_HEADER] = "Can't read file header\n",
        [READ_INVALID_BMP_SIZE] = "Invalid biSize\n",
        [READ_INVALID_IMG_SIZE] = "Invalid img size\n",
        [READ_NULL_POINTER] = "Null pointer exception\n",
        [READ_INVALID_BODY] = "Can't read file body\n"
};

#endif //ASSIGNMENT_3_IMAGE_ROTATION_STATUS_BMP_PRINTS_H
